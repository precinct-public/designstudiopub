# DesignStudioPub



## Install the Design Studio

To install the design studio component on a local machine please follow the above instructions:

1. Clone the repository
2. Navigate to the folder and run <docker-compose up --build> command (The task will take a few minutes)
3. When the container is built get the container_id
4. Navigate to the container using <docker exec -it "container_id" /bin/bash> command
5. Run the following commands:
    <br />a. npm install grunt
    <br />b. apt install -y git
    <br />c. bower install --allow-root
    <br />d. grunt build
    <br />e. grunt serve
6. Open a browser and navigate to http://0.0.0.0:8088

